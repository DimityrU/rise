﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOPNatureReserveSimulationSolution.Food
{
    public enum FoodEnum
    {
        None,
        Meat,
        Grass,
        Leaf,
        Corn,
        Barley,
        Granules, 
        Worm
    }

}
