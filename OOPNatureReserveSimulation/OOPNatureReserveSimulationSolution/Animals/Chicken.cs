﻿using OOPNatureReserveSimulationSolution.Food;
using System.Collections.Generic;

namespace OOPNatureReserveSimulationSolution.Animals
{
    public class Chicken : Animal
    {
        public Chicken(int maxValue) : base(maxValue, new HashSet<Enum> { FoodEnum.Corn, FoodEnum.Barley, FoodEnum.Worm })
        {
            Energy = maxValue;
        }
    }
}
