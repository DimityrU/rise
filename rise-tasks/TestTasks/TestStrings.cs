﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using rise_tasks;
using System;

namespace TestTasks
{
    [TestClass]
    public class TestStrings
    {
        [TestMethod]

        public void TestReverseString()
        {

        }


        [TestMethod]
        public void TestReverseEveryWord()
        {
            String input = "What is this";
            string expected = "tahW si siht";

            String actual =  Strings.ReverseEveryWord(input);

            Assert.AreEqual(expected, actual);
        }

        [TestMethod]

        public void TestReverseEveryWordOneWord()
        {
            String input = "What";
            string expected = "tahW";

            String actual = Strings.ReverseEveryWord(input);

            Assert.AreEqual(expected, actual);
        }

        [TestMethod]

        public void TestReverseEveryWordNoWord()
        {
            String input = "";
            string expected = "";

            String actual = Strings.ReverseEveryWord(input);

            Assert.AreEqual(expected, actual);
        }

        [TestMethod]

        public void TestIsPalindrome()
        {
            bool expected = Strings.IsPalindrome("noon");

            Assert.IsTrue(expected);
        }

        [TestMethod]

        public void TestIsPalindromeFalse()
        {
            bool expected = Strings.IsPalindrome("no");

            Assert.IsFalse(expected);
        }

        [TestMethod]

        public void TestIsPalindromeNum()
        {
            bool expected = Strings.IsPalindrome(44);

            Assert.IsTrue(expected);
        }

        [TestMethod]

        public void TestIsPalindromeNumFalse()
        {
            bool expected = Strings.IsPalindrome(50);

            Assert.IsFalse(expected);
        }
    }
}
